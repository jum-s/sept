# Play sept in the terminal

Open a terminal and clone the repository on your machine:

`git clone git@gitlab.com:jum-s/sept.git`

Then go in the created folder

`cd sept`

This little program is written in python, only because this langage is natively installed on most Linux system. You can then run the next command and enjoy your game:

`python run.py`

There is also a ruby version https://gitlab.com/jum-s/rubysept