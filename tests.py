from game import Game
import unittest

class GameTest(unittest.TestCase):
  def start_unshuffled_game(self):
      game = Game()
      # do not shuffle the game
      game.start(False)
      return(game)

  def start_shuffled_game(self):
      game = Game()
      # do not shuffle the game
      game.start()
      return(game)

  def test_attempt_fail(self):
      game = self.start_unshuffled_game()
      cards = [1,2,4]
      result = game.play_attempt(cards)
      self.assertIn(result, "Hum...")

  def test_attempt_set(self):
      game = self.start_unshuffled_game()
      cards = [1,2,3]
      result = game.play_attempt(cards)
      self.assertIn(result, "Set !")

  def test_no_set_found(self):
      game = self.start_shuffled_game()
      rounds = 0
      while True:
          if rounds < 20:
              rounds += 1
              hint = game.get_hint()
              cards_positions = game.sanitize(hint)
              if len(cards_positions) is 3:
                  game.play_attempt(cards_positions)
              else:
                  self.assertIn(hint, "No set found!")
                  break
          else:
            game = self.start_shuffled_game()
            rounds = 0

unittest.main()
