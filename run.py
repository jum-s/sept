from game import Game

game = Game()
game.start()
print(game.tapis.display())
print("")

while len(game.deck.cards) > 3:
    raw_player_attempt = raw_input('Pick your set : (n) new game, (q) quit, (a) add 3 cards, (h) hint\n')

    if raw_player_attempt is 'q':
        break
    elif raw_player_attempt is 'a':
        game.add_cards()
        print(game.tapis.display())
    elif raw_player_attempt is 'n':
        game.start()
    elif raw_player_attempt is 'h':
        print(game.tapis.display())
        set_letters = game.get_hint()
        print("It's our little secret: " + set_letters)
    else:
        player_attempt = game.sanitize(raw_player_attempt)
        if not len(set(player_attempt)) is 3:
            result = "Invalid input. Should be 3 distinct letters between a & l (ex: 'abc' or 'gfe')."
        else:
            result = game.play_attempt(player_attempt)
        print(game.tapis.display())
        print(result)
