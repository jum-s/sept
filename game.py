import string
import random
import itertools
from tapis import Tapis
from deck import Deck
from attempt import Attempt

class Game:
    def __init__(self):
        self.tapis = Tapis()
        self.deck = Deck()

    def start(self, shuffle = True):
        if shuffle:
            self.deck.cards = random.sample(self.deck.cards, len(self.deck.cards))
        self.tapis.deal(self.deck)

    def try_attempt(self, cards):
        attempt = Attempt(cards)
        if attempt.is_set():
            self.get_new_cards(cards)
        return(attempt)

    def get_new_cards(self, set_cards):
        new_tapis = []
        if len(self.tapis.cards) > 12:
            for set_card in set_cards:
                self.tapis.cards.remove(set_card)
            return()
        for tapis_card in self.tapis.cards:
            if tapis_card in set_cards:
                new_card = self.deck.cards.pop()
                new_tapis.append(new_card)
            else:
                new_tapis.append(tapis_card)
        self.tapis.cards = new_tapis

    def add_cards(self):
        if len(self.tapis.cards) < 15:
            self.tapis.add_cards(self.deck)

    def get_hint(self):
        set_possibilities = itertools.combinations(self.tapis.cards, 3)
        set_letters = []
        for set_possibility in set_possibilities:
            attempt = Attempt(set_possibility)
            if attempt.is_set():
                for card in set_possibility:
                    position = self.tapis.cards.index(card) + 1
                    letter = self.tapis.to_letter(position)
                    set_letters.append(letter)
                break
        if set_letters is []:
            return("no set found!")
        return("".join(set_letters))

    def get_cards(self, player_input):
        attempt = []
        for card_num in player_input:
            card = self.tapis.cards[card_num - 1]
            attempt.append(card)
        return(attempt)

    def play_attempt(self, player_attempt):
        cards = self.get_cards(player_attempt)
        attempt = self.try_attempt(cards)
        return(attempt.resolve())


    def sanitize(self, player_input):
        attempt = []
        for card_letter in player_input:
            if self.to_number(card_letter) > 15:
                return(error_message)
        for a in player_input:
            number = self.to_number(a)
            attempt.append(number)
        return(attempt)

    def to_number(self, letter):
        letters = list(string.ascii_lowercase)
        number = letters.index(letter)
        if isinstance(number, int):
            return(number + 1)
