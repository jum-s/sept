import string

class Tapis:
    def __init__(self):
        self.cards = []

    def deal(self, deck):
        for i in range(12):
           card = deck.cards.pop()
           self.cards.append(card)

    def display(self):
        tapis_result = []
        if len(self.cards) is 12:
            table = [[0,1,2,3], [4,5,6,7], [8, 9,10,11]]
        else:
            table = [[0,1,2,3,4], [5,6,7,8,9], [10,11,12,13,14]]

        for pos in table:
            result = []
            for po in pos:
                result.append(self.cards[po])
            tapis_result.append(self.build_row(result))

        return("\n".join(tapis_result))

    def build_row(self, cards):
        row = []
        lines_count = len(cards[0].card_display())
        for line_index in range(lines_count) :
            row.append(self.build_line(cards, line_index))

        return("\n".join(row))

    def build_line(self, row_cards, line_index):
        line = []
        cards_count = len(row_cards)
        if line_index is 1:
            for card in row_cards:
                position = self.cards.index(card) + 1
                letter = self.to_letter(position)
                original_display = list(card.card_display()[line_index])
                original_display[3] = letter
                line.append("".join(original_display))
        else:
            for card in row_cards:
                line.append(card.card_display()[line_index])

        return("".join(line))

    def to_letter(self, position):
        return(list(string.ascii_lowercase)[position - 1])

    def add_cards(self, deck):
        for i in range(3):
            new_card = deck.cards.pop()
            self.cards.append(new_card)
