class Attempt:
    def __init__(self, cards):
        self.cards = cards
        self.criterias = ['color', 'texture', 'number', 'form']

    def resolve(self):
        if self.is_set():
            return("Set !")
        else:
            return("Hum...")

    def is_set(self):
        criterias_in_common = []
        for criteria in self.criterias:
            criterias_in_common.append(self.number_of_same(criteria))
        possibilities = [[1, 1, 1, 3], [1, 1, 3, 3], [1, 3, 3, 3], [3, 3, 3, 3]]
        return(sorted(criterias_in_common) in possibilities)

    def number_of_same(self, criteria):
        result = []
        for card in self.cards:
            result.append(getattr(card, criteria))
        return(len(list(set(result))))
