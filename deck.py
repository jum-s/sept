import itertools
from card import Card

class Deck:
    def __init__(self):
        self.create_cards()

    def create_cards(self):
        colors = [ '31', '32', '34' ]
        textures = [ 'p', 'e', 'l' ]
        forms = [ 'O', '~', '#' ]
        numbers = [ '1', '2', '3' ]
        cards_product = itertools.product(colors, textures, forms, numbers)
        self.cards = []
        for card_signs in cards_product:
            self.cards.append(Card(card_signs))
