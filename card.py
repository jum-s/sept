# -*- coding: UTF-8 -*-

class Card:
    def __init__(self, signs):
        self.color = signs[0]
        self.texture = signs[1]
        self.form = signs[2]
        self.number = signs[3]

    def card_display(self):
        framed_content = []
        for line in self.lines_content():
            framed_content.append(''.join(["│",line,"║"]))
        display = []
        display.append("┌─────────────────╖")
        display += framed_content
        display.append("│                 ║")
        display.append("╘═════════════════╝")

        return(display)

    def lines_content(self):
        empty_line = "                 "
        if self.number == "3":
            return([empty_line] + self.colored_shape() + [empty_line] + self.colored_shape() + [empty_line] + self.colored_shape())
        elif self.number == "2":
            return([empty_line, empty_line, empty_line] + self.colored_shape() + [empty_line] + self.colored_shape() + [empty_line, empty_line])
        elif self.number == "1":
            return([empty_line, empty_line, empty_line, empty_line, empty_line] + self.colored_shape() + [empty_line, empty_line, empty_line, empty_line])

    def colored_shape(self):
        display = []
        for ascii_display in self.ascii_displays():
            display.append(''.join(["\033[", self.color, "m", ascii_display, "\033[0m" ]))
        return(display)

    def ascii_displays(self):
        if self.texture == "e":
            if self.form == "~":
                return([
                   "     ╱▔▔╲▁╱▔╲    ",
                   "    ╱       ╱    ",
                   "    ╲▁╱▔╲▁▁╱     "
                ])
            elif self.form == "O":
                return([
                   "    ╱▔▔▔▔▔▔▔╲    ",
                   "    ▏       ▕    ",
                   "    ╲▁▁▁▁▁▁▁╱    "
                ])
            elif self.form == "#":
                return([
                   "   ▕▔▔▔▔▔▔▔▔▏    ",
                   "   ▕        ▏    ",
                   "   ▕▁▁▁▁▁▁▁▁▏    "
                ])
        elif self.texture == "p":
            if self.form == "~":
                return([
                    "     d8b  d8     ",
                    "    d888888P     ",
                    "    8P  Y8P      "
                ])
            elif self.form == "O":
                return([
                    "     d88888b     ",
                    "    888888888    ",
                    "     Y888888     "
                ])
            elif self.form == "#":
                return([
                    "    88888888     ",
                    "    88888888     ",
                    "    88888888     "
                ])
        elif self.texture == "l":
            if self.form == "~":
                return([
                    "     ▟█▙  ▟▙     ",
                    "    ▟██████▛     ",
                    "    ▜▛  ▜█▛      "
                ])
            elif self.form == "O":
                return([
                    "     ▟█████▙     ",
                    "    █████████    ",
                    "     ▜█████▛     "
                ])
            elif self.form == "#":
                return([
                    "    ████████     ",
                    "    ████████     ",
                    "    ████████     "
                ])
